import matplotlib.pyplot as plt 
import pandas as pd 
import numpy as np 


# Line plot
x1 = [2008,2009,2010,2011,2012]
y1 = [8,12,10,14,18]
plt.plot(x1, y1, 'g.--', label='Movies watched',markersize=20,markeredgecolor='blue')
font = {'family': 'serif', 'color': 'darkred', 'weight': 'normal', 'size': 16}
plt.title("Movies watched by Kim's family", fontdict=font)
plt.xlabel('Year')
plt.ylabel("Number of Movies")
plt.xticks([2008,2009,2010,2011,2012])
plt.yticks([6,8,10,12,14,16,18,20])
plt.savefig('movies_Watched.png', dpi=100)
plt.legend()
plt.show()

# 
x2 = [2008,2009,2010,2011,2012,2013]
y2 = [700,500,550,645,300,200]
y3 = [650,700,480,800,200,300]
plt.plot(x2, y2, 'g.--', label='Island Middle School',markersize=20,markeredgecolor='blue')
plt.plot(x2, y3, 'c+-', label='Eden Middle School',markersize=20,markeredgecolor='orange')
plt.title("Island vs Eden collection of Aluminium Beverage Cans", fontdict=font)
plt.xlabel('Year')
plt.ylabel("Weight of cans in pounds")
plt.xticks([2008,2009,2010,2011,2012,2013])
plt.yticks([200,300,400,500,600,700,800,900])
plt.savefig('Aluminum_beverage_cans.png', dpi=100)
plt.legend()
plt.show()

# bar graph (bar() - for vertical bars, barh() - for horizontal bars)
x4 = ['Ant', 'Lady Bug', 'Grasshopper', 'Spider']
count = [3,10,5,8]
bar_color = ['magenta', 'cyan', 'orange', 'pink']
plt.barh(x4, count, color = bar_color )
plt.title("Jason's Insects", fontdict=font)
plt.xlabel('Total Count')
plt.ylabel("Insect Type")
plt.xticks([1,2,3,4,5,6,7,8,9,10])
plt.savefig("Jason's Insects.png", dpi=100)
plt.show()

# Pie Graph
activity = ['Sandcastle building','Volleyball','Collecting seashells','Surfing','Kite flying']
percent = [21,15,17,30,17]
plt.pie(percent, labels = activity)
plt.title("Mrs. Carolyn's Class Favorite Beach Activities")
plt.savefig("Beach Activities.png", dpi=100)
plt.show()