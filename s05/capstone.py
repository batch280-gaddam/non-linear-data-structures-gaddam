student_list = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
student_grade = [60, 80, 95, 85, 89, 97]

import pandas as pd 

gradebook = dict(
    {
        "student_list": student_list,
        "student_grade": student_grade
    }
) 

print(gradebook)

data_frame = pd.DataFrame(gradebook, index=[1,2,3,4,5,6])
print(data_frame)
print('')

def continue_to_menu():
    # print(input('\nChoose an option '))
    # print(input('\nNew Student Name '))
    # print(input('\nNew Student grade '))

    # gradebook['student_list'] = input('New Student Name ')
    # gradebook['student_grade'] = input('New Student Grade ')

    print(input('\nPress ENTER to continue'))
    print(gradebook)
    main_menu()

def main_menu():
    menu = """
    [program begins]
    Menu:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Lookup a student's grade
    5. Find student with highest grade
    6. Find student with lowest grade
    7. Generate graph
    8. Quit
    """
    print(menu)

            
main_menu()
continue_to_menu()

print(gradebook['student_list'])
print(gradebook['student_grade'])

for item in gradebook:
    if student_name in student_list:
        print(student_grade)