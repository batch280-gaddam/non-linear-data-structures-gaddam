# using the library for binary tree

import binarytree as bt 
from binarytree import build 

nodes = [3,6,8,2,11,13]
binary_tree = build(nodes) #build() function builds a new binary tree with the nodes specified

print('Binary Tree from list: \n', binary_tree) #Prints the whole binary tree
print('\n List from binary tree: ', binary_tree.values) #prints onlt the values of the binary tree

print('Tree in inorder traversal: ', binary_tree.inorder)
print('Tree in preorder traversal: ', binary_tree.preorder)
print('Tree in postorder traversal: ', binary_tree.postorder)

print('The size of the tree is: ', binary_tree.size)

print('The height of the tree is: ', binary_tree.height)
print('The amount of leaves in the tree are: ', binary_tree.leaf_count)
print('The levelf of the tree: ', binary_tree.levels)