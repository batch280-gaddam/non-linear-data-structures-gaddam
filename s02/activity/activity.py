import binarytree as bt 
# from binarytree import build 

# nodes = [1,2,3,4,5]
# binary_tree = build(nodes)
# print('BInary', binary_tree)

from binarytree import Node 
root = Node(1)
root.left = Node(2)
root.right = Node(2)
root.left.right = Node(4)
root.right.left = Node(4)
print(root)
print('Is this a balanced tree? ', root.is_balanced)
print('Is this a symmetrical tree? ', root.is_symmetric)
print("Height of the balanced tree: ", root.height)

root.left.left = Node(6)
root.right.right = Node(7)
print(root)
print('Is this BST? ', root.is_bst)
print('Is the BST strict? ', root.is_strict)
print('The min heap is: ', root.min_node_value)
print('The max heap is: ', root.max_node_value)

# root.right.left = Node(5)
root.right = Node(3)
del root[3]
root.left.left = Node(6)

print(root)
print('Is this a complete tree? ', root.is_complete)
print('The leaf count is: ', root.leaf_count)
print('The levels of the tree: ', root.levels)