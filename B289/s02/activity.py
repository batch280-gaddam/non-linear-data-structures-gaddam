# 1. Create an edge list representation of the 5 alpha codes corresponding to 5 states in the US
# 	CA is connected to:
# 		MA; kilometers: 50
# 	FL is connected to:
# 		CA; kilometers: 25
# 	NY is connected to the following:
# 		WA; kilometers: 70
# 		NJ; kilometers: 41
# 	WA is connected to:
# 		FL; kilometers: 67
# 	NJ is connected to the following:
# 		WA; kilometers: 34
# 		NY; kilometers: 41
# 	Create a class to be instantiated with the necessary methods for adding and printing edges.
# 	instantiate the class, add the edges of the graph
# 	print the edges with their corresponding weight/kilometers

class Graph: 
	# constructor
	def __init__(self, directed=True):
		self.m_directed = directed

		self.m_list_of_edges = []

	# main function - for adding two vertices and an edge weight to the list of the edges, regardless if the graph is directed or not
	def add_edge(self, node1, node2, weight=1):
		self.m_list_of_edges.append([node1, node2, weight])

		if not self.m_directed:
			self.m_list_of_edges.append([node1, node2, weight])

	# To display the graph as an Edge List
	# For printing the edge list by looping through each value
	def print_edge_list(self):
		for i in range(len(self.m_list_of_edges)):
			print("Edge ", i+1, ": ", self.m_list_of_edges[i])

graph = Graph()

graph.add_edge('CA', 'MA', 50)  #(vertex, vertex, weight)
graph.add_edge('FL', 'CA', 25)
graph.add_edge('NY', 'WA', 70)
graph.add_edge('MA', 'NY', 31)
graph.add_edge('NJ', 'WA', 34)
graph.add_edge('WA', 'FL', 67)
graph.add_edge('NY', 'NJ', 41)

print('distances among the 5 states in the US ')
graph.print_edge_list()




# 2. Based on this class:
# 	Create an undirected instance of the graph class
# 	Add edges to the graph class along with their weights
# 	Print the edges of the instance.
import pandas as pd

class Graph:
    def __init__(self, num_of_nodes, n, directed=True):
        self.m_num_of_nodes = num_of_nodes
        self.m_directed = directed

        self.m_adj_matrix = [[0 for column in range(num_of_nodes)]
                            for row in range(num_of_nodes)]

        self.adj_df = pd.DataFrame(self.m_adj_matrix, columns=[c for c in n], index=[r for r in n])
    
    def add_edge(self, node1, node2, weight=1):
        self.adj_df[node2][node1] = weight

        if not self.m_directed:
            self.adj_df[node1][node2] = weight

    def print_adj_matrix(self):
            print(self.adj_df)

n = [0, 1, 2, 3, 4]

graph = Graph(5, n, directed=True)

graph.add_edge(0, 0, 0)
graph.add_edge(0, 1, 8)
graph.add_edge(0, 2, 10)
graph.add_edge(1, 0, 8)
graph.add_edge(1, 3, 4)
graph.add_edge(1, 4, 11)
graph.add_edge(2, 0, 10)
graph.add_edge(2, 4, 2)
graph.add_edge(3, 1, 4)
graph.add_edge(3, 4, 12)
graph.add_edge(4, 1, 11)
graph.add_edge(4, 1, 2)
graph.add_edge(4, 3, 12)
graph.add_edge(4, 4, 3)

print('adjacent matrix ')
graph.print_adj_matrix()


# 3. Create a class to demonstrate adjacency list representation of a directed graph.
# 		- Its constructor will accept a list of nodes that will be used to create a linked list
# 		- Add the necessary methods for adding and printing edges of the graph
# 		- Add a functionality that will print the degree of a vertex.
# 			- The vertex should be passed as an argument
# 	Instantiate the class created.
# 	Pass a list of nodes in instantiating the graph class
# 	Add edge to the graph and assign weights to them
# 	Print the graph representation in adjacency list format
# 	Print the degree of a vertex of the graph

class Graph:
	def __init__(self, nodes, directed=True):
		self.m_directed = directed
		self.m_adj_list = {node: list() for node in nodes}

	def add_edge(self, node1, node2, weight=1):
		self.m_adj_list[node1].append((node2, weight))

		if not self.m_directed:
			if node1 == node2:
				pass 
			else:
				self.m_adj_list[node1].append((node1, weight))

	def print_adj_list(self):
		for key in self.m_adj_list.keys():
			print("node", key, ": ", self.m_adj_list[key])

n = [ 1, 2, 3, 4, 5]

graph = Graph(n)

# graph.add_edge(0, 2, 3)
# graph.add_edge(0, 0, 25)
# graph.add_edge(0, 1, 5)
graph.add_edge(2, 1, 1)
# graph.add_edge(1, 3, 15)
graph.add_edge(4, 2, 1)
graph.add_edge(4, 3, 1)
graph.add_edge(5, 5, 1)
graph.add_edge(5, 1, 1)
graph.add_edge(5, 3, 1)

print('directed adjacency list ')
graph.print_adj_list()


# Stretch goal:
# For the adjacency list instances, create a method for adding and printing the edge that will create a loop, connecting a node to itself.


class Graph:
	def __init__(self, nodes, directed=False):
		self.m_directed = directed
		self.m_adj_list = {node: list() for node in nodes}

	def add_edge(self, node1, node2, weight=1):
		self.m_adj_list[node1].append((node2, weight))

		if not self.m_directed:
			if node1 == node2:
				pass 
			else:
				self.m_adj_list[node1].append((node1, weight))

	def print_adj_list(self):
		for key in self.m_adj_list.keys():
			print("node", key, ": ", self.m_adj_list[key])

n = [ 1, 2, 3, 4, 5]

graph = Graph(n)

# graph.add_edge(0, 2, 3)
# graph.add_edge(0, 0, 25)
# graph.add_edge(0, 1, 5)
graph.add_edge(2, 1, 1)
# graph.add_edge(1, 2, 1)
graph.add_edge(4, 2, 1)
graph.add_edge(4, 3, 1)
graph.add_edge(5, 5, 1)
graph.add_edge(5, 1, 1)
graph.add_edge(5, 3, 1)

print('undirected adjacency list ')
graph.print_adj_list()