# ACTIVITY INSTRUCTIONS
# 1. Create a function for recommending friends that will accept a graph and a user parameters.
    # Create user_friends variable that contains the set of the neighbors of the user.
    # Create recommended_friends that contains an empty set.
    # Using breadth-first search, iterate through the graph to see which people in the social network is not yet connected to the user
    # If they are not yet connected to the user, add them to the recommended_friends set.
    # Return the recommended_friends

# from queue import Queue

# class Graph:
#     # Constructor
#     def __init__(self, num_of_nodes, directed=True): 
#         self.m_directed = directed

#         self.m_adj_list = {node: list() for node in num_of_nodes}  
            
#     # Add edge to the graph
#     def add_edge(self, node1, node2, weight=1):
#         self.m_adj_list[node1].append((node2, weight))

#         if not self.m_directed:
#             self.m_adj_list[node2].append((node1, weight))
    
#     # Print the graph representation
#     def print_adj_list(self):
#       for key in self.m_adj_list.keys():
#         print("node", key, ": ", self.m_adj_list[key])

#     def recommending_friends(graph, user_parameters):
#         user_friends = set()
#         recommended_friends = set()

#     # BSF(Breadth-First Search) algorithm
#     def bfs_traversal(self, start_node):
#         visited = set() #setting/Initializing an empty array for visited nodes
#         queue = Queue() #Initializing a queue for the remaining nodes

#         queue.put(start_node) #puts statr_node inside the queue
#         visited.add(start_node) #Also adds start_node at the 'visited' list

#         while not queue.empty():
#             current_node = queue.get() #Returns the first value in a queue

#             print(current_node, end=" ") #Prints the value of current_node

#             # Loops through the list and puts the next_node in the queue and vice versa
#             for(next_node, weight) in self.m_adj_list[current_node]:
#                 if next_node not in visited:
#                     queue.put(next_node)
#                     visited.add(next_node)


# n = [0, 1, 2, 3, 4]

# graph = Graph(n, directed=False)

# graph.add_edge('John', 'Emma')
# graph.add_edge('Tohn', 'Chris')
# graph.add_edge('Emma', 'Kate')
# graph.add_edge('Chris', 'Kate')
# graph.add_edge('Chris', 'Michael')
# graph.add_edge('Kate', 'Michael')

# # graph.print_adj_list()
# print('BFS Traversal ')
# graph.bfs_traversal()


class GetSuggestedFriends:
    #Constructor, Time O(1) Space O(1)
    def __init__(self) :
        self.adj = {}
        self.groups = []
    
    #Add edges, Time O(1) Space O(1)
    def addFriendship(self, a, b) :    
        if a not in self.adj:
            self.adj[a] = []
        if b not in self.adj:
            self.adj[b] = []        
        self.adj[a].append(b)
        self.adj[b].append(a)       
    
    # Find groups by connections, DFS, Time O(V+E), Space O(V)
    def findGroups(self) : 
        visited = {}
        for t in self.adj.keys():
            if visited.get(t) == None:
                group = set()
                self.dfs(t, visited, group)
                self.groups.append(group)
 
    #DFS helper, Time O(V+E), Space O(V) 
    def dfs(self, v, visited, group) :
        visited[v] = True
        group.add(v)
        for ne in self.adj[v] :
            if ne not in visited:
                self.dfs(ne, visited, group)
 
    #Find suggested friends, Time O(V+E), Space O(V)
    def getSuggestedFriends (self, a) :
        if len(self.groups) == 0:
            self.findGroups()
        res = set()
        for  t in self.groups: 
            if a in t:               
                res = t;             
                break
        # if len(res) &gt; 0: #remove himself from suggested friends
        #     res.remove(a)
        for t in self.adj.get(a):
            res.remove(t)
        return res
        
g = GetSuggestedFriends()
g.addFriendship("John", "Emma")
g.addFriendship("John", "Chris")
g.addFriendship("Emma", "Kate")
g.addFriendship("Chris", "Kate")
g.addFriendship("Chris", "Michael")
g.addFriendship("Kate", "Michael")
 
name = "John"
print("Suggestion friends of " + name + ": ")
print(g.getSuggestedFriends(name)); 
 
# name = "Sam"
# print("Suggestion friends of " + name + ": ")
# print(g.getSuggestedFriends(name)); 




# 2. Instantiate the graph class using the load_graph function and pass “friends.txt” as its argument.
# 3. Using the function for recommending friends, recommend friend/s for a user in the graph.
# 4. Print the recommended friends of the user in the console
# 5. Create a git repository named graph-theory-capstone
# 6. Add the link to local repository and push to git repository with the commit message of “Add Graph Theory Capstone”
# 7. Add the link in Boodle


# Function to load the graph from a file
def load_graph(file_path):
    graph = nx.Graph()
    with open(file_path, 'r') as f:
        for line in f:
            edge = line.strip().split(',')
            graph.add_edge(edge[0], edge[1])
    return graph

# Stretch goal:
# Create an env folder inside the capstone folder.
# Create a method for visualizing the social network using networkx and matplotlib packages. 
# Using the method, plot and create an image of the graph.
