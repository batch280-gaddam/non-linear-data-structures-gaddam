# ACTIVITY INSTRUCTIONS
# 1. Create a function for recommending friends that will accept a graph and a user parameters.
    # Create user_friends variable that contains the set of the neighbors of the user.
    # Create recommended_friends that contains an empty set.
    # Using breadth-first search, iterate through the graph to see which people in the social network is not yet connected to the user
    # If they are not yet connected to the user, add them to the recommended_friends set.
    # Return the recommended_friends
# 2. Instantiate the graph class using the load_graph function and pass “friends.txt” as its argument.
# 3. Using the function for recommending friends, recommend friend/s for a user in the graph.
# 4. Print the recommended friends of the user in the console
# 5. Create a git repository named graph-theory-capstone
# 6. Add the link to local repository and push to git repository with the commit message of “Add Graph Theory Capstone”
# 7. Add the link in Boodle


# Function to load the graph from a file
def load_graph(file_path):
    graph = nx.Graph()
    with open(file_path, 'r') as f:
        for line in f:
            edge = line.strip().split(',')
            graph.add_edge(edge[0], edge[1])
    return graph

# Stretch goal:
# Create an env folder inside the capstone folder.
# Create a method for visualizing the social network using networkx and matplotlib packages. 
# Using the method, plot and create an image of the graph.
