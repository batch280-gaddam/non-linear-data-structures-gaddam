from heapq import heapify, heappush, heappop, heapreplace, nlargest, nsmallest

min_heap = []

heapify(min_heap)

def print_min_heap():
	print("Min-heap elements: ")

	for i in min_heap:
		print(i, end=' ')
	print('\n')

heappush(min_heap, 10)
heappush(min_heap, 24)
heappush(min_heap, 28)
heappush(min_heap, 44)
heappush(min_heap, 40)

# print_min_heap()
print('min heap: ' , min_heap)
print('largest values: ', nlargest(3, min_heap))
dela = nsmallest(1, min_heap)
print(dela)
print('deleted item: ', heappop(min_heap))

print('new min_heap: ', min_heap ) 

max_heap = []

heapify(max_heap)

heappush(max_heap, -1 * 10)
heappush(max_heap, -1 * 30)
heappush(max_heap, -1 * 400)
heappush(max_heap, -1 * 700)
heappush(max_heap, -1 * 20)

def print_max_heap():
	print("Max-heap elements: ")
	for i in max_heap:
		print((-1 * i), end=' ')
	print('\n')

# print_max_heap()
print('max heap: ', max_heap)

print('2 largest: ', nlargest(2, max_heap))
print('3 smallest: ', nsmallest(3, max_heap))
# sort the max_heap